#!/usr/bin/env python3

from derpibooru import Search
import os
import argparse
import re
import fileinput
import json
import bunker_api as api
import urllib.request
import requests
import uuid

workdir = os.path.dirname(os.path.abspath(__file__)) + '/'
config = open(workdir + 'config.json', mode='r', encoding='utf_8', newline='\n')
configText = ""
for i in config:
    configText += i
configJson = json.loads(configText)


def login_bunker():
    return api.User(configJson["bunker"]["login"], configJson["bunker"]["password"])


def upload_to_imgur(link):
    post_fields = {'image': link}
    headers = {"Authorization": "Client-ID " + configJson["imgur"]["login"]}
    return json.loads(
        requests.post(url="https://api.imgur.com/3/image", data=post_fields, headers=headers).text
    )["data"]["link"]


def upload_to_foxtrot(link):
    post_fields = {'file': link}
    return "https://filecave.frumscepend.com/" + json.loads(
        requests.post(url="https://filecave.frumscepend.com/link", data=post_fields).text
    )["response"]


def upload_bunker(user, link):
    return 'https:' + user.upload_image(link)


def get_booru_info(booru_id):
    # Главная функция
    key = configJson["dirpibooru"]["key"]
    out = {}
    for image in Search().key(key).query("id:" + booru_id):
        out['booru'] = image.url
        src = image.source_url
        if src is None:
            out['source'] = ''
        else:
            out['source'] = src
        out['tags'] = image.tags
        out['full'] = image.full  # image.py: "https:{}" убрал, теперь просто "{}"
        out['fulltags'] = image.representations['full']  # ссылка на полный размер с тегами в имени файла
        out['large'] = image.large
        out['tall'] = image.tall
        out['medium'] = image.medium
        out['small'] = image.small
        out['id'] = image.id
        out['thumb'] = image.thumb
        out['thumb_small'] = image.thumb_small
        out['thumb_tiny'] = image.thumb_tiny
    if out == {}:
        out['booru'] = booru_id
        out['source'] = 'warning: image deleted'
        out['large'] = 'warning: image deleted'
        out['full'] = 'warning: image deleted'
        out['tags'] = ['warning', 'image deleted']
        out['tall'] = 'warning: image deleted'
        out['id'] = booru_id
        out['fulltags'] = 'warning: image deleted'
    return out


def trytoupload(link):
    if upflag:
        try:
            if configJson["imgur"]["login"] == "":
                # link = upload_bunker(usr, upload_to_foxtrot(link))
                link = upload_bunker(usr, link)
            else:
                link = upload_bunker(usr, upload_to_imgur(link))
        except:
            print("Ошибка загрузки. Используется оригинальная ссылка.")
    return link


# Получаем словарик тегов из файла или по url
rutags = {}
if configJson["remote_tags"]["enabled"]:
    tagResponse = urllib.request.urlopen(configJson["remote_tags"]["url"])
    tagData = tagResponse.read()  # a `bytes` object
    tagText = tagData.decode('utf-8')  # a `str`; this step can't be used if data is binary
    for i in tagText.splitlines():
        i = i.rstrip()
        a = i.split(sep=',')
        rutags.update({a[0]: a[1]})
else:
    file = open(workdir + 'rtags.txt', mode='r', encoding='utf_8', newline='\n')
    for i in file:
        i = i.rstrip()
        a = i.split(sep=',')
        rutags.update({a[0]: a[1]})
# Парсер аргументов коммандной строки
parser = argparse.ArgumentParser()
parser.add_argument('-t', '--tabunmode', action='store_true',
                    help='Использовать табунский вариант спойлеров, иначе бункерский.')
parser.add_argument('-u', '--upload', action='store_true',
                    help='Загружать каждую картинку на Бункер.')
parser.add_argument('-i', '--inpdir', action='store',
                    help='Входная директория.')
parser.add_argument('-g', '--grid', action='store',
                    help='Вывести картинки в таблицу.')
parser.add_argument('-r', '--read', action='store', nargs='?', const='-',
                    help='Читать номера картинок не из имен файлов в папке, а из ссылок на картинки, поданных на стандартный ввод или файл.')
parser.add_argument('-l', '--lamp', action='store', nargs='?', const='comm',
                    help='Изменить маркер безопасности. "circ" - круглые, "sqr" - квадратные, "post" - высокие линии под пост, "comm" - низкие линии под коммент.')
parser.add_argument('-s', '--spoiler', action='store_true',
                    help='Учитывать теги "spoiler:..".')
parser.add_argument('-n', '--numeration', action='store', nargs='?', const='default',
                    help='Изменить нумерацию. "no" - нет нумерации, "default" - нумерация, если количество картинок больше пяти, "always" - всегда нумеровать.')
parser.add_argument('-w', '--wrapper', action='store', nargs='?', const='default',
                    help='Изменить спойлер-обертку. "no" - нет обертки, "default" - обертка появляется, если количество картинок больше пяти, "always" - всегда оборачивать.')
parser.add_argument('-wt', '--wrapper-text', action='store', nargs='?', const='pics',
                    help='Изменить текст на спойлере-обертке, по умолчанию там "pics".')
parser.add_argument('-p', '--previewmode', action='store', nargs='?', const='small',
                    help='Режим, в котором вместо тегов на спойлерах ставятся превьюшки с Буры. "thumb" - размером 250, "small" - 150, "tiny" - 50')
parser.add_argument('-z', '--size', action='store', nargs='?', const='large',
                    help='Размер картинки. По умолчанию large, для комиксов tall. Доступны: thumb, thumb_tiny, thumb_small, small, medium, large, tall, full')
parser.add_argument('-d', '--download', action='store_true',
                    help='Режим скачивания картинок по ссылкам. Не делается никаких спойлеров, картинки просто скачиваются в папку dwl.')

# Режим таблицы
gridmode = False
gridtags = ''
if parser.parse_args().grid:
    gridmode = True
if parser.parse_args().tabunmode:
    tabunmode = True
else:
    tabunmode = False

# Режим загрузки на Бункер
if parser.parse_args().upload:
    '''if input('Вы действительно хотите грузить все на Бункер?').lower() == 'y':
        upflag = True
        usr = login_bunker()
    else:
        upflag = False'''
    upflag = True
    usr = login_bunker()
else:
    upflag = False

# Получение списка айдишников
uplist = []
if parser.parse_args().inpdir:
    uplist = os.listdir(parser.parse_args().inpdir)  # Из имен файлов
else:
    if parser.parse_args().read:  # Из ссылок
        for line in fileinput.input(parser.parse_args().read):
            # вычленить номер из url
            a = re.match('https?:\/\/(derpi|trixie)booru.org\/(images\/)?(\d+)', line)
            b = re.match('https?:\/\/derpicdn.net\/img\/(view\/)?\d{4}\/\d{1,}\/\d{1,}\/(\d+)', line)
            if a:
                num = a.group(3)
            elif b:
                num = b.group(2)
            else:
                num = 'error'
            uplist = uplist + [num + '_']
    else:
        uplist = os.listdir(workdir + 'upl')
# uplist.sort() # сортировка была нужна для таблицы, пока выключено
print(uplist, '\n')
ni = 0
print_list = []
if not gridmode:
    for i in range(len(uplist)):
        print_list.append([''] * 3)  # Создание пустого списка для принтлиста в случае табличного режима
col_counter = 0
for i in uplist:
    if i[0] == '.':
        uplist.remove(i)  # Удалить имена файлов, начинающиеся с точки из списка

# Наборы лампочек
safety_tags = ['safe', 'suggestive', 'questionable', 'explicit']
circ_lamps = ['static.lunavod.ru/img/063a6cf977f9953d342405f8bcdfc21fcb0d1f6a.png',
              'static.lunavod.ru/img/4396da788b9e82f2741034185c34a069aab28c3c.png',
              'static.lunavod.ru/img/bc135991534b6078a11df6ce816ca5906846f0df.png',
              'static.lunavod.ru/img/8c3cf8826399a8c88e399c0fbe3ebe872081d1b2.png']
sqr_lamps = ['static.lunavod.ru/img/c2d6e5242a390326dcd3f8142979f02707a550da.png',
             'static.lunavod.ru/img/949c52dd084b9bd5cb8890c3db52b56708642685.png',
             'static.lunavod.ru/img/4cba1c9b7b3a962d6c9af55400af5a64f1e4a3a4.png',
             'static.lunavod.ru/img/7b5dea934ccf0cd249c299d155256658a94539c3.png']
post_line_lamps = ['static.lunavod.ru/img/65a2de52946cd13117a7303424de242244999978.png',
                   'static.lunavod.ru/img/e769ddc5d3f6aa2c9fe3f6c3858498b0ce2d0ff4.png',
                   'static.lunavod.ru/img/17c068cf543d15e3dcfd8d98f126a02df97edfb8.png',
                   'static.lunavod.ru/img/709cd7aacaae60c36ecedf915535e06ed2947b56.png']
comm_line_lamps = ['static.lunavod.ru/img/ba28fd072d2fa0e5d3bde423a82f78e5cbe083ec.png',
                   'static.lunavod.ru/img/98c78a499b5880db1b67cfe345ec5e2e0e3dd549.png',
                   'static.lunavod.ru/img/f5ab355a47ba1bcac7f410ea5b15fa314908a5b6.png',
                   'static.lunavod.ru/img/f3f75b48d65619ac55e51c81815feeb860250447.png']
prslamp = parser.parse_args().lamp
if prslamp == 'comm':
    lamps = comm_line_lamps
elif prslamp == 'post':
    lamps = post_line_lamps
elif prslamp == 'circ':
    lamps = circ_lamps
elif prslamp == 'sqr':
    lamps = sqr_lamps
else:
    lamps = comm_line_lamps

# Режим превьюшек
plotspoiler = 'https://static.lunavod.ru/img/e91e6a69d61e3d08531a16dcae8c2458831d861a.png'  # картинка с Рэрити, которая кричит "СПОЙЛЕР!"
rarispoiler = 'https://static.lunavod.ru/img/f753972ebfb187c609603960107d13d137687575.png'  # safe
trixiespoiler = 'https://static.lunavod.ru/img/03550f2c09af51f1b3dbf24ce6616f2ea016500c.png'  # suggestive
twilyspoiler = 'https://static.lunavod.ru/img/93d01aa876632d8889a64c334fb27045ce58165b.png'  # questionable
applespoiler = 'https://static.lunavod.ru/img/2e30a2c8f07f0248d53e693d232567bcffcdcde4.png'  # explicit
semidarkspoiler = 'https://static.lunavod.ru/img/1f79dfbad49262e61f911cc15fc241b1c3a868b4.png'  # semi-grimdark
darkspoiler = 'https://static.lunavod.ru/img/b0eecc869f469054139a75fd1126eb3315902cfd.png'  # grimdark
grotspoiler = 'https://static.lunavod.ru/img/1c1661cc4a4c2912bf3c155aefe58b24fe524e90.png'  # grotesque
premode = parser.parse_args().previewmode  # Если параметра нет, то переменная получит None, значит не включать режим превьюшек
priorities = ['grotesque', 'explicit', 'grimdark', 'semi-grimdark', 'questionable', 'suggestive']
takespoiler = [grotspoiler, applespoiler, darkspoiler, semidarkspoiler, twilyspoiler, trixiespoiler, rarispoiler]
prespoiler = len(priorities)
addition = ''
spoiltext = ''

# Главный цикл
for i in uplist:
    if i.find('_') > 0:
        bid = i[:i.find('_')]  # Получение айдишника из имени файла
    else:
        bid = i[:i.find('.')]  # Короткое имя - айдишник до точки
    if not bid.isdigit():
        bid = 'error'
    if bid != "error":
        info = get_booru_info(bid)
        if info['fulltags'] == 'warning: image deleted':
            print('{}: image deleted or unreachable'.format(bid))
            continue
        if parser.parse_args().download:  # режим скачивания
            if info['fulltags'] == 'warning: image deleted':
                print('{}: image deleted or unreachable'.format(bid))
            else:
                outfilename = info['fulltags'][info['fulltags'].index(bid):]
                print('Downloading:', bid)
                with open("dwl\\" + outfilename, 'wb') as out_stream:
                    req = requests.get(info['fulltags'], stream=True)
                    for chunk in req.iter_content(1024):  # Куски по 1 КБ
                        out_stream.write(chunk)
            continue
        if 'comic' in info['tags']:
            linktopic = info['tall']
        else:
            linktopic = info['medium']
        if parser.parse_args().size:
            linktopic = info[parser.parse_args().size]
        linktopic = trytoupload(linktopic)
        img = '<img src="{}">'.format(linktopic)
        if info['source'] != '':
            author = '<a href="{}" target="_blank"><strong>[&nbsp;Автор&nbsp;]</strong></a>'.format(info['source'])
        else:
            author = ''
        booru = '<a href="{}" target="_blank"><strong>[&nbsp;Бура&nbsp;]</strong></a>'.format(info['booru'])
        fullsize = '<a href="{}" target="_blank"><strong>[&nbsp;Скачать&nbsp;]</strong></a>'.format(
            info['fulltags']).replace('view', 'download')
        if not premode:
            my_tags = []
            lamp = '()'

            # Исключения по русскому тегованию
            if 'underwear' in info['tags'] and 'lingerie' in info['tags']:
                info['tags'].remove('underwear')
            if 'lesbian' in info['tags'] and 'safe' in info['tags']:
                info['tags'].remove('lesbian')
            if 'straight' in info['tags'] and 'safe' in info['tags']:
                info['tags'].remove('straight')
            if ('equestria girls' in info['tags'] or 'humanized' in info['tags']) and 'clothes' in info['tags']:
                info['tags'].remove('clothes')
            if 'male' in info['tags'] and 'stallion' in info['tags']:
                info['tags'].remove('male')
            if 'pinkamena diane pie' in info['tags'] and 'pinkie pie' in info['tags']:
                info['tags'].remove('pinkie pie')
            if 'woona' in info['tags'] and 'princess luna' in info['tags']:
                info['tags'].remove('princess luna')

            # Создание русских тегов
            for tag in info['tags']:
                if tag in rutags:
                    my_tags.append(rutags[tag])  # Из словаря
                if tag.find('oc:') >= 0:
                    my_tags.append('ОС:' + tag[3:].title())  # Из имени ОСа
                if tag.find('artist:') >= 0:
                    my_tags.append('автор:' + tag[7:].title())  # Из имени художника
                if tag.find('spoiler:') >= 0:
                    my_tags.append('cпойлер:' + tag[8:].title())  # Из имени спойлера даже без режима спойлеров
                if parser.parse_args().spoiler:
                    if tag.find('spoiler:') >= 0:
                        img = img.replace('<img', '<img class="gray"')
                for i in range(0, 4):
                    if tag == safety_tags[i]:
                        lamp = '<img src="https://{}">&nbsp;'.format(lamps[i])  # Создание лампочек
            my_tags.sort()
            if not my_tags:
                my_tags.append('теги не поставились')
            tags = ', '.join(my_tags)
            if parser.parse_args().spoiler:
                if tags.find('cпойлер:') >= 0:
                    tags = '{}{}<span class="spoiler-gray">{}</span>'.format(tags[:tags.find('cпойлер:')],
                                                                             tags[tags.find('cпойлер:'):][
                                                                             :tags[tags.find('cпойлер:'):].find(
                                                                                 ',') + 2],
                                                                             tags[tags.find('cпойлер:'):][
                                                                             tags[tags.find('cпойлер:'):].find(
                                                                                 ',') + 2:])
        else:  # Превьюшки вместо тегов
            if premode == 'thumb':
                preimg = info['thumb']
            elif premode == 'small':
                preimg = info['thumb_small']
                addition = ' width="150"'  # добавка с шириной к img
            elif premode == 'tiny':
                preimg = info['thumb_tiny']
                addition = ' width="50"'
            if parser.parse_args().spoiler:
                for tag in info['tags']:
                    if tag.find('spoiler:') >= 0:
                        preimg = plotspoiler
                        img = img.replace('<img', '<img class="gray"')
                        spoiltext = ' title="Cпойлер к {}!"'.format(tag[8:].title())
                if preimg != plotspoiler:
                    preimg = trytoupload(preimg)
                    preimg = '<img src="{}">'.format(preimg)
                else:
                    preimg = '<img src="{}"{}{}>'.format(preimg, addition, spoiltext)
            else:
                preimg = trytoupload(preimg)
                preimg = '<img src="{}">'.format(preimg)
            for tag in info['tags']:
                if tag in priorities:
                    if priorities.index(tag) < prespoiler:
                        prespoiler = priorities.index(tag)

        # Создание спойлера
        if gridmode:
            gridtags = gridtags + lamp + tags + '\n'
            if col_counter == 0:
                spoiler = '<tr>'
            spoiler = spoiler + '<td>' + img + '\n' + author + ' ' + booru + ' ' + fullsize + '</td>'
            col_counter = col_counter + 1
            if col_counter >= int(parser.parse_args().grid) or ni == len(uplist) - 1:
                spoiler = spoiler + '</tr>'
                col_counter = 0
        else:
            if premode:
                spoiler = ('<span class="spoiler"><span class="spoiler-title spoiler-close">{}</span>' +
                           '<span class="spoiler-body">{}\n{} {} {}</span></span>').format(preimg, img, author, booru,
                                                                                           fullsize)
            else:
                spoiler = ('<span class="spoiler"><span class="spoiler-title spoiler-close">{}{}</span>' +
                           '<span class="spoiler-body">{}\n{} {} {}</span></span>\n').format(lamp, tags, img, author,
                                                                                             booru, fullsize)
        if tabunmode:
            spoiler = spoiler.replace(' spoiler-close', '')
        if gridmode:
            if col_counter == 0:
                print_list.append([0, 0, spoiler])
        else:
            for k in info['tags']:
                if k.find('artist:') >= 0:
                    break
            print_list[ni][0] = k
            print_list[ni][1] = int(info['id'])
            print_list[ni][2] = spoiler
        print(ni, info['id'])
    ni += 1

for i in print_list:
    if i == ['', '', '']:
        print_list.remove(i)
block_uuid = uuid.uuid4().hex
print('<a name="{}"></a>'.format(block_uuid), end='')  # пишем якорь начала нашего блока картинок
wrapit = False
wrapparam = parser.parse_args().wrapper
if wrapparam == None:
    wrapparam = "default"
if wrapparam == "default":
    if len(print_list) > 5:
        wrapit = True
elif wrapparam == "always":
    wrapit = True
elif wrapparam == "no":
    wrapit = False
wraptext = parser.parse_args().wrapper_text
if wraptext == None:
    wraptext = "pics"
if wrapit:
    if premode:
        if prespoiler != len(priorities):
            prespoiler = takespoiler[prespoiler]
            s = '<span class="spoiler"><span class="spoiler-title spoiler-close"><img src="{}"{}>\n{}</span><span class="spoiler-body">'.format(
                prespoiler, addition, wraptext)
        else:
            s = '<span class="spoiler"><span class="spoiler-title spoiler-close">{}</span><span class="spoiler-body">'.format(
                wraptext)
    else:
        s = '<span class="spoiler"><span class="spoiler-title spoiler-close">{}</span><span class="spoiler-body">'.format(
            wraptext)
    if tabunmode:
        s = s.replace(' spoiler-close', '')
    print(s, end='')
else:
    if premode:
        if prespoiler != len(priorities):
            prespoiler = takespoiler[prespoiler]
            s = '<span class="spoiler"><span class="spoiler-title spoiler-close"><img src="{}"{}></span><span class="spoiler-body">'.format(
                prespoiler, addition)
            if tabunmode:
                s = s.replace(' spoiler-close', '')
            print(s, end='')
if gridmode:
    s = '<span class="spoiler"><span class="spoiler-title spoiler-close">' + gridtags + '</span><span class="spoiler-body"><table>'
    if tabunmode:
        s = s.replace(' spoiler-close', '')
    print(s, end='')
numerate = False
if not gridmode:
    print_list.sort()
    numparam = parser.parse_args().numeration
    if numparam == None:
        numparam = "default"
    if numparam == "default":
        if len(print_list) > 5:
            numerate = True
        if premode:  # в режиме с превьюшками numerate нужен только если его явно включат через always
            numerate = False
    elif numparam == "always":
        numerate = True
    elif numparam == "no":
        numerate = False

k = 0
if numerate:
    print('<table>', end='')
for i in range(len(print_list)):
    kk = k
    if numerate:
        toprint = '<tr><td>{}.&nbsp;</td><td>{}</td></tr>'.format(i + 1, print_list[i][2])
    else:
        toprint = print_list[i][2]
    k = k + len(toprint)
    if k >= 190000:
        print(
            '\n*** k={} будет на следующей строчке, а сейчас {} ====================================================== ***\n'.format(
                k, kk))
        k = 0
    print(toprint, end='')
if numerate:
    print('</table>', end='')
if gridmode:
    if wrapit:
        print('</table></span></span>', end='')
    else:
        print('</table>\n<a href="#{}">Наверх &uarr;</a></span></span>'.format(block_uuid),
              end='')  # ссылка для перехода наверх блока картинок, она пишется либо в конце спойлера таблицы...
if wrapit:
    print('\n<a href="#{}">Наверх &uarr;</a>'.format(block_uuid), end='')  # ...либо в конце спойлера-обертки...
    print('</span></span>', end='')
else:
    if not gridmode:
        print('\n<a href="#{}">Наверх &uarr;</a>'.format(block_uuid),
              end='')  # ...либо просто в конце блока, если нет никаких оборачивающих спойлеров.
    if premode:
        if prespoiler != len(priorities):
            print('</span></span>', end='')
