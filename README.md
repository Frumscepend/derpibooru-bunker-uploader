Файл `config.json` должен лежать в корневой папке.

Стандартная конфигурация:   
`{
"bunker":{
	"login":"",
	"password":""
	},
"dirpibooru":{
	"key":""
	},
"imgur":{
	"login":"8d89e80d4160a18",
	"password":"a41ba7b958919e9340f56fc0a48fc47e16cf639a"
	},
"proxy":{
	"url":"http://proxy.antizapret.prostovpn.org:3128",
	"enabled":true
	},
"remote_tags":{
	"url":"https://gitlab.com/Tolyas/derpibooru-bunker-uploader/raw/master/rtags.txt",
	"enabled":true
	}
}`